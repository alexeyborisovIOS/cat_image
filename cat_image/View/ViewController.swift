//
//  ViewController.swift
//  cat_image
//
//  Created by Борисов Алексей on 03.06.2020.
//  Copyright © 2020 Борисов Алексей. All rights reserved.
//

import UIKit

class ViewController: UICollectionViewController{
    
    var petsImage: [UIImage] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        let width = (view.frame.size.width-10)/2
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: width)
        // Do any additional setup after loading the view.
        urlSession()
    }
    
    func urlSession() {
        let urlString = "https://api.thecatapi.com/v1/images/search?limit=10"
        
        guard let url = URL(string: urlString) else {return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let response = response {
                print(response)
            }
            
            guard let data = data else {return}
            guard error == nil else { return }
            
            do {
                let petsApi = try JSONDecoder().decode([ModelApi].self, from: data)
                for i in 0..<petsApi.count {
                    self.setImage(from: petsApi[i].url!)
                }
            } catch let error {
                print(error)
            }
        }.resume()
    }
    
    func setImage(from url: String) {
        guard let imageURL = URL(string: url) else { return }

        // just not to cause a deadlock in UI!
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }

            let image = UIImage(data: imageData)
            self.petsImage.append(image!)
        }
    }

    @IBAction func reload(_ sender: Any) {
        collectionView.reloadData()
        urlSession()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return petsImage.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
        cell.image.image = petsImage[indexPath.row]
        cell.image.backgroundColor = UIColor.white
        return cell
    }
}

