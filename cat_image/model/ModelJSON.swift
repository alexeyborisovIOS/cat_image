//
//  ModelJSON.swift
//  cat_image
//
//  Created by Борисов Алексей on 16.06.2020.
//  Copyright © 2020 Борисов Алексей. All rights reserved.
//

import Foundation

struct ModelApi: Decodable {
    var id : String?
    var height: Int?
    var url: String?
    var width: Int?
}

