//
//  CollectionViewCell.swift
//  cat_image
//
//  Created by Борисов Алексей on 03.06.2020.
//  Copyright © 2020 Борисов Алексей. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    
}
